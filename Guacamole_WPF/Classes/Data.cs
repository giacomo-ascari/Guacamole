﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows;

namespace Guacamole_WPF.Classes
{
    public class Data : List<Point>
    {
        private string name;
        private bool visible;
        private Color color;
        private bool compressed;

        public string Name { get => name; set => name = value; }
        public bool Visible { get => visible; set => visible = value; }
        public Color Color { get => color; set => color = value; }
        public bool Compressed { get => compressed; set => compressed = value; }
        public int Length
        {
            get
            {
                if (this.Count > 0)
                {
                    return (int)(this[this.Count - 1].X - this[0].X + 1);
                }
                else return 0;
            }
        }
        public string Compression
        {
            get
            {
                return (int)(Math.Abs((double)Count / Length - 1) * 100.0) + "%";
            }
        }
        /*public double Average
        {
            get
            {
                double average = 0;
                foreach (double item in this) average += item;
                return average / this.Count;
            }
        }*/
        public double MinY
        {
            get
            {
                if (this.Count > 0)
                {
                    double min = this[0].Y;
                    foreach (Point item in this) if (item.Y < min) min = item.Y;
                    return min;
                }
                else return 0;
            }
        }
        public double MaxY
        {
            get
            {
                if (this.Count > 0)
                {
                    double max = this[0].Y;
                    foreach (Point item in this) if (item.Y > max) max = item.Y;
                    return max;
                }
                else return 0;
            }
        }

        public Data()
        {
            this.color = Color.Black;
        }

        public Data(string name, bool visible, Color color, bool compressed) : this()
        {
            this.name = name;
            this.visible = visible;
            this.color = color;
            this.compressed = compressed;
        }

        public Data(string name) : this()
        {
            this.name = name;
        }

        /*public static Data CalcRatio(Data raw, double num)
        {
            Data final = new Data(raw.name);
            for (int i = 0; i < raw.Count; i++)
            {
                final.Add(num / raw[i] - 1);
            }
            return final;
        }*/
        /*public static Data CalcBound(Data raw, double upper, double lower)
        {
            Data final = new Data(raw.name);
            foreach (double value in raw)
            {
                if (value <= upper && value >= lower)
                {
                    final.Add(value);
                }
                else if (value > upper)
                {
                    final.Add(upper);
                }
                else if (value < lower)
                {
                    final.Add(lower);
                }
            }
            return final;
        }*/
        /*public static Data CalcDerive(Data raw, int span, bool alterCount)
        {
            Data final = new Data(raw.name);
            if (raw.Count >= span)
            {
                if (alterCount)
                {
                    for (int i = 0; i < raw.Count; i += span)
                    {
                        if (i + span < raw.Count)
                        {
                            final.Add((raw[i + span] - raw[i]) / span);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < raw.Count; i++)
                    {
                        if (i + span < raw.Count)
                        {
                            final.Add((raw[i + span] - raw[i]) / span);
                        }
                    }
                }
            }
            else
            {
                final = null;
            }
            return final;
        }*/

        /*public static Data CalcAverage(Data raw, int span, bool alterCount)
        {
            Data final = new Data(raw.name);

            if (raw.Count >= span)
            {
                if (alterCount)
                {
                    for (int i = 0; i < raw.Count; i += span)
                    {
                        double avg = 0;
                        int y;
                        for (y = 0; y < span && i + y < raw.Count; y++)
                        {
                            avg += raw[i + y];
                        }
                        final.Add(avg / y);
                    }
                }
                else
                {
                    for (int i = 0; i < raw.Count; i++)
                    {
                        double avg = 0;
                        int y;
                        for (y = 0; y < span && i + y < raw.Count; y++)
                        {
                            avg += raw[i + y];
                        }
                        final.Add(avg / y);
                    }
                }
            }
            else
            {
                final = null;
            }
            return final;
        }*/

        public bool Compress()
        {
            bool success = false;
            if (!this.compressed && Count > 2)
            {
                double a = this[0].Y;

                int i = 1;
                while (i < Count - 1)
                {
                    if (this[i].Y == a && a == this[i + 1].Y)
                    {
                        this.RemoveAt(i);
                    } else
                    {
                        a = this[i].Y;
                        i++;
                    }
                }
                compressed = true;
                success = true;
            }
            return success;
        }
        
        public static bool TryParse(string text, out Data data)
        {
            try
            {
                string[] a = text.Split(';');
                string[] b = a[4].Split(',');
                List<double> list = new List<double>();
                data = new Data(a[0], a[1]=="True", Color.FromArgb(Int32.Parse(a[2])), Boolean.Parse(a[3]));
                foreach (string item in b)
                {
                    string s = item.Replace('.', ',');
                    data.Add(Point.Parse(s));
                }
                return true;
            }
            catch
            {
                data = null;
                return false;
            }
        }

        public static Data Parse(string text)
        {
            Data data = null;
            if (TryParse(text, out data))
            {
                return data;
            }
            else
            {
                throw new InvalidOperationException("Invalid format for Data object");
            }
        }

        public override string ToString()
        {
            string text = string.Format("{0};{1};{2};{3};", name, visible, color.ToArgb(), compressed);
            foreach (Point item in this)
            {
                text += item.ToString().Replace(',', '.') + ",";
            }
            text = text.Substring(0, text.Length - 1);
            return text;
        }
    }

    public enum DataFormat
    {
        None,
        F0,
        F1,
        F2,
        F3,
        F6,
        F9,
        F12,
        E0,
        E1,
        E2,
        E3,
        E6,
        E9,
        E12,
    }
}
