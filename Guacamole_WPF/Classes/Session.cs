﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.ComponentModel;

namespace Guacamole_WPF.Classes
{
    public class Session : INotifyPropertyChanged
    {
        private DataSet dataSet;
        private string fileName;

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public Session(DataSet dataSet, string fileName) : this()
        {
            this.dataSet = dataSet;
            this.fileName = fileName;
        }

        public Session()
        {
            dataSet = new DataSet();
            fileName = "";
        }

        public DataSet DataSet { get => dataSet; set { dataSet = value; NotifyPropertyChanged("DataSet"); } }
        public string FileName { get => fileName; set { fileName = value; NotifyPropertyChanged("FileName"); } }

        public static Session Parse(string text)
        {
            Session session = new Session();
            if (TryParse(text, out session))
            {
                return session;
            }
            else
            {
                throw new FormatException("String format error");
            }
        }

        static public bool Writable(Session session)
        {
            if (session.FileName.Length > 0 && File.Exists(session.FileName))
            {
                return true;
                /*try
                {
                    FileStream fs = new FileStream(session.FileName, FileMode.Open, FileAccess.Write);
                    fs.Close();
                    return true;
                }
                catch
                {
                    return false;
                }*/
            }
            return false;
        }

        static public bool Save(Session session)
        {
            try
            {
                StreamWriter sw = new StreamWriter(session.fileName, false);
                sw.AutoFlush = true;
                sw.Write(session.ToString());
                sw.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static Session Load(string fileName)
        {
            try
            {
                Session session = new Session() { FileName = fileName };
                StreamReader sr = new StreamReader(fileName);
                while (!sr.EndOfStream)
                {
                    session.DataSet.Add(Data.Parse(sr.ReadLine()));
                }
                sr.Close();
                return session;
            }
            catch
            {
                return null;
            }
            
        }

        public static bool TryParse(string text, out Session session)
        {
            session = new Session();
            try
            {
                string[] v = text.Trim().Split('\n');
                foreach (string item in v)
                {
                    session.dataSet.Add(Data.Parse(item));
                }
                return true;
            }
            catch
            {
                session = null;
                return false;
            }
        }

        public override string ToString()
        {
            string text = "";
            for (int i = 0; i < dataSet.Count; i++)
            {
                text += dataSet[i].ToString();
                if (i != dataSet.Count -1)
                {
                    text += "\n";
                }
            }
            return text;
        }
    }
}
