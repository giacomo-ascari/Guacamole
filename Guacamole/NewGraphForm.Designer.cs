﻿namespace Guacamole
{
    partial class NewGraphForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewGraphForm));
            this.cancelBtn = new System.Windows.Forms.Button();
            this.okBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.xCBox = new System.Windows.Forms.ComboBox();
            this.yCBox = new System.Windows.Forms.ComboBox();
            this.yLbl = new System.Windows.Forms.Label();
            this.xLbl = new System.Windows.Forms.Label();
            this.visibleCBox = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.colorBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.nameTxt = new System.Windows.Forms.TextBox();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cancelBtn
            // 
            this.cancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelBtn.Location = new System.Drawing.Point(207, 198);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 0;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            // 
            // okBtn
            // 
            this.okBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okBtn.Enabled = false;
            this.okBtn.Location = new System.Drawing.Point(126, 198);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(75, 23);
            this.okBtn.TabIndex = 1;
            this.okBtn.Text = "OK";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.xCBox);
            this.groupBox1.Controls.Add(this.yCBox);
            this.groupBox1.Controls.Add(this.yLbl);
            this.groupBox1.Controls.Add(this.xLbl);
            this.groupBox1.Controls.Add(this.visibleCBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.colorBtn);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.nameTxt);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(269, 179);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add a new graph";
            // 
            // xCBox
            // 
            this.xCBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.xCBox.FormattingEnabled = true;
            this.xCBox.Location = new System.Drawing.Point(6, 121);
            this.xCBox.Name = "xCBox";
            this.xCBox.Size = new System.Drawing.Size(121, 21);
            this.xCBox.TabIndex = 9;
            this.xCBox.SelectedValueChanged += new System.EventHandler(this.content_Changed);
            // 
            // yCBox
            // 
            this.yCBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.yCBox.FormattingEnabled = true;
            this.yCBox.Location = new System.Drawing.Point(6, 148);
            this.yCBox.Name = "yCBox";
            this.yCBox.Size = new System.Drawing.Size(121, 21);
            this.yCBox.TabIndex = 8;
            this.yCBox.SelectedValueChanged += new System.EventHandler(this.content_Changed);
            // 
            // yLbl
            // 
            this.yLbl.AutoSize = true;
            this.yLbl.Location = new System.Drawing.Point(133, 151);
            this.yLbl.Name = "yLbl";
            this.yLbl.Size = new System.Drawing.Size(36, 13);
            this.yLbl.TabIndex = 7;
            this.yLbl.Text = "Y Axis";
            // 
            // xLbl
            // 
            this.xLbl.AutoSize = true;
            this.xLbl.Location = new System.Drawing.Point(133, 124);
            this.xLbl.Name = "xLbl";
            this.xLbl.Size = new System.Drawing.Size(35, 13);
            this.xLbl.TabIndex = 6;
            this.xLbl.Text = "X axis";
            // 
            // visibleCBox
            // 
            this.visibleCBox.AutoSize = true;
            this.visibleCBox.Location = new System.Drawing.Point(6, 75);
            this.visibleCBox.Name = "visibleCBox";
            this.visibleCBox.Size = new System.Drawing.Size(56, 17);
            this.visibleCBox.TabIndex = 4;
            this.visibleCBox.Text = "Visible";
            this.visibleCBox.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(110, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Color";
            // 
            // colorBtn
            // 
            this.colorBtn.Location = new System.Drawing.Point(6, 46);
            this.colorBtn.Name = "colorBtn";
            this.colorBtn.Size = new System.Drawing.Size(100, 23);
            this.colorBtn.TabIndex = 2;
            this.colorBtn.UseVisualStyleBackColor = true;
            this.colorBtn.Click += new System.EventHandler(this.colorBtn_Click);
            this.colorBtn.Paint += new System.Windows.Forms.PaintEventHandler(this.colorBtn_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(110, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name";
            // 
            // nameTxt
            // 
            this.nameTxt.Location = new System.Drawing.Point(6, 19);
            this.nameTxt.Name = "nameTxt";
            this.nameTxt.Size = new System.Drawing.Size(100, 20);
            this.nameTxt.TabIndex = 0;
            this.nameTxt.TextChanged += new System.EventHandler(this.content_Changed);
            // 
            // NewGraphForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(294, 233);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.cancelBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "NewGraphForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Graph creation";
            this.Load += new System.EventHandler(this.NewGraphForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label yLbl;
        private System.Windows.Forms.Label xLbl;
        private System.Windows.Forms.CheckBox visibleCBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button colorBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nameTxt;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.ComboBox xCBox;
        private System.Windows.Forms.ComboBox yCBox;
    }
}