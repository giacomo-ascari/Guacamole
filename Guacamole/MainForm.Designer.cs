﻿namespace Guacamole
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataDGV = new System.Windows.Forms.DataGridView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.saveTSBtn = new System.Windows.Forms.ToolStripButton();
            this.loadTSBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.addDataListTSBtn = new System.Windows.Forms.ToolStripDropDownButton();
            this.newTSBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.appendTSBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.addGraphTSBtn = new System.Windows.Forms.ToolStripDropDownButton();
            this.graph2dTSBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.graph3dTSBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.editTSBtn = new System.Windows.Forms.ToolStripButton();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.infoDGV = new System.Windows.Forms.DataGridView();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.graphDGV = new System.Windows.Forms.DataGridView();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.graphBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataDGV)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.infoDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.graphDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.graphBox)).BeginInit();
            this.SuspendLayout();
            // 
            // dataDGV
            // 
            this.dataDGV.AllowUserToAddRows = false;
            this.dataDGV.AllowUserToDeleteRows = false;
            this.dataDGV.AllowUserToResizeRows = false;
            this.dataDGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataDGV.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dataDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.NullValue = "-";
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataDGV.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataDGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataDGV.Location = new System.Drawing.Point(3, 117);
            this.dataDGV.MultiSelect = false;
            this.dataDGV.Name = "dataDGV";
            this.dataDGV.ReadOnly = true;
            this.dataDGV.RowHeadersWidth = 60;
            this.dataDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataDGV.Size = new System.Drawing.Size(516, 374);
            this.dataDGV.TabIndex = 0;
            this.dataDGV.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.tableDGV_CellFormatting);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveTSBtn,
            this.loadTSBtn,
            this.toolStripSeparator1,
            this.addDataListTSBtn,
            this.addGraphTSBtn,
            this.editTSBtn});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1159, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // saveTSBtn
            // 
            this.saveTSBtn.Image = ((System.Drawing.Image)(resources.GetObject("saveTSBtn.Image")));
            this.saveTSBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveTSBtn.Name = "saveTSBtn";
            this.saveTSBtn.Size = new System.Drawing.Size(51, 22);
            this.saveTSBtn.Text = "Save";
            this.saveTSBtn.Click += new System.EventHandler(this.saveTSBtn_Click);
            // 
            // loadTSBtn
            // 
            this.loadTSBtn.Image = ((System.Drawing.Image)(resources.GetObject("loadTSBtn.Image")));
            this.loadTSBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.loadTSBtn.Name = "loadTSBtn";
            this.loadTSBtn.Size = new System.Drawing.Size(53, 22);
            this.loadTSBtn.Text = "Load";
            this.loadTSBtn.Click += new System.EventHandler(this.loadTSBtn_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // addDataListTSBtn
            // 
            this.addDataListTSBtn.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newTSBtn,
            this.appendTSBtn});
            this.addDataListTSBtn.Image = ((System.Drawing.Image)(resources.GetObject("addDataListTSBtn.Image")));
            this.addDataListTSBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addDataListTSBtn.Name = "addDataListTSBtn";
            this.addDataListTSBtn.Size = new System.Drawing.Size(76, 22);
            this.addDataListTSBtn.Text = "Add list";
            // 
            // newTSBtn
            // 
            this.newTSBtn.Image = ((System.Drawing.Image)(resources.GetObject("newTSBtn.Image")));
            this.newTSBtn.Name = "newTSBtn";
            this.newTSBtn.Size = new System.Drawing.Size(116, 22);
            this.newTSBtn.Text = "New list";
            this.newTSBtn.Click += new System.EventHandler(this.newTSBtn_Click);
            // 
            // appendTSBtn
            // 
            this.appendTSBtn.Image = ((System.Drawing.Image)(resources.GetObject("appendTSBtn.Image")));
            this.appendTSBtn.Name = "appendTSBtn";
            this.appendTSBtn.Size = new System.Drawing.Size(116, 22);
            this.appendTSBtn.Text = "Append";
            this.appendTSBtn.Click += new System.EventHandler(this.appendTSBtn_Click);
            // 
            // addGraphTSBtn
            // 
            this.addGraphTSBtn.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.graph2dTSBtn,
            this.graph3dTSBtn});
            this.addGraphTSBtn.Image = ((System.Drawing.Image)(resources.GetObject("addGraphTSBtn.Image")));
            this.addGraphTSBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addGraphTSBtn.Name = "addGraphTSBtn";
            this.addGraphTSBtn.Size = new System.Drawing.Size(92, 22);
            this.addGraphTSBtn.Text = "Add graph";
            // 
            // graph2dTSBtn
            // 
            this.graph2dTSBtn.Image = ((System.Drawing.Image)(resources.GetObject("graph2dTSBtn.Image")));
            this.graph2dTSBtn.Name = "graph2dTSBtn";
            this.graph2dTSBtn.Size = new System.Drawing.Size(122, 22);
            this.graph2dTSBtn.Text = "2D graph";
            this.graph2dTSBtn.Click += new System.EventHandler(this.graph2dTSBtn_Click);
            // 
            // graph3dTSBtn
            // 
            this.graph3dTSBtn.Enabled = false;
            this.graph3dTSBtn.Image = ((System.Drawing.Image)(resources.GetObject("graph3dTSBtn.Image")));
            this.graph3dTSBtn.Name = "graph3dTSBtn";
            this.graph3dTSBtn.Size = new System.Drawing.Size(122, 22);
            this.graph3dTSBtn.Text = "3D graph";
            // 
            // editTSBtn
            // 
            this.editTSBtn.Image = ((System.Drawing.Image)(resources.GetObject("editTSBtn.Image")));
            this.editTSBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editTSBtn.Name = "editTSBtn";
            this.editTSBtn.Size = new System.Drawing.Size(85, 22);
            this.editTSBtn.Text = "Process list";
            this.editTSBtn.Click += new System.EventHandler(this.editTSBtn_Click);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "Guacamole DataSet | *.gds";
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Guacamole DataSet | *.gds";
            // 
            // infoDGV
            // 
            this.infoDGV.AllowUserToAddRows = false;
            this.infoDGV.AllowUserToDeleteRows = false;
            this.infoDGV.AllowUserToResizeRows = false;
            this.infoDGV.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.infoDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.infoDGV.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.infoDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.infoDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.infoDGV.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.infoDGV.Location = new System.Drawing.Point(3, 3);
            this.infoDGV.MultiSelect = false;
            this.infoDGV.Name = "infoDGV";
            this.infoDGV.RowHeadersVisible = false;
            this.infoDGV.RowHeadersWidth = 60;
            this.infoDGV.Size = new System.Drawing.Size(516, 108);
            this.infoDGV.TabIndex = 0;
            this.infoDGV.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.infoDGV_CellBeginEdit);
            this.infoDGV.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.infoDGV_CellFormatting);
            this.infoDGV.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.infoDGV_CellMouseClick);
            this.infoDGV.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.infoDGV_CellValueChanged);
            this.infoDGV.CurrentCellDirtyStateChanged += new System.EventHandler(this.infoDGV_CurrentCellDirtyStateChanged);
            // 
            // colorDialog
            // 
            this.colorDialog.SolidColorOnly = true;
            // 
            // graphDGV
            // 
            this.graphDGV.AllowUserToAddRows = false;
            this.graphDGV.AllowUserToDeleteRows = false;
            this.graphDGV.AllowUserToResizeRows = false;
            this.graphDGV.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.graphDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.graphDGV.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.graphDGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.graphDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.graphDGV.Location = new System.Drawing.Point(3, 497);
            this.graphDGV.MultiSelect = false;
            this.graphDGV.Name = "graphDGV";
            this.graphDGV.RowHeadersVisible = false;
            this.graphDGV.RowHeadersWidth = 60;
            this.graphDGV.Size = new System.Drawing.Size(516, 122);
            this.graphDGV.TabIndex = 2;
            this.graphDGV.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.graphDGV_CellClick);
            this.graphDGV.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.graphDGV_CellPainting);
            this.graphDGV.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.graphDGV_CellValueChanged);
            // 
            // splitContainer
            // 
            this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer.Location = new System.Drawing.Point(13, 29);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.infoDGV);
            this.splitContainer.Panel1.Controls.Add(this.graphDGV);
            this.splitContainer.Panel1.Controls.Add(this.dataDGV);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.graphBox);
            this.splitContainer.Size = new System.Drawing.Size(1134, 622);
            this.splitContainer.SplitterDistance = 522;
            this.splitContainer.SplitterWidth = 8;
            this.splitContainer.TabIndex = 3;
            this.splitContainer.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer_SplitterMoved);
            this.splitContainer.SizeChanged += new System.EventHandler(this.splitContainer_SizeChanged);
            // 
            // graphBox
            // 
            this.graphBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.graphBox.BackColor = System.Drawing.Color.White;
            this.graphBox.Location = new System.Drawing.Point(4, 4);
            this.graphBox.Name = "graphBox";
            this.graphBox.Size = new System.Drawing.Size(593, 615);
            this.graphBox.TabIndex = 0;
            this.graphBox.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1159, 663);
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Guacamole";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataDGV)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.infoDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.graphDGV)).EndInit();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.graphBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataDGV;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton saveTSBtn;
        private System.Windows.Forms.ToolStripButton loadTSBtn;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStripDropDownButton addDataListTSBtn;
        private System.Windows.Forms.DataGridView infoDGV;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.DataGridView graphDGV;
        private System.Windows.Forms.ToolStripDropDownButton addGraphTSBtn;
        private System.Windows.Forms.ToolStripMenuItem graph2dTSBtn;
        private System.Windows.Forms.ToolStripMenuItem graph3dTSBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem newTSBtn;
        private System.Windows.Forms.ToolStripMenuItem appendTSBtn;
        private System.Windows.Forms.ToolStripButton editTSBtn;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.PictureBox graphBox;
    }
}