﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guacamole
{
    class GraphSet : List<Plot>
    {
        public GraphSet()
        {

        }

        public override string ToString()
        {
            string text = string.Empty;
            foreach (Plot item in this)
            {
                text += item.ToString() + "\n";
            }
            return text;
        }

        public bool FindOtherNames(string name)
        {
            bool other = true;
            int pos1 = this.FindIndex(x => x.Name == name);
            int pos2 = this.FindLastIndex(x => x.Name == name);
            if (pos1 == pos2) other = false;
            return other;
        }

        public void ChangeDataListNames(string oldName, string newName)
        {
            for (int i = 0; i < this.Count; i++)
            {
                for (int j = 0; j < this[i].Names.Count; j++)
                {
                    if (this[i].Names[j] == oldName)
                    {
                        this[i].Names[j] = newName;
                    }
                }
            }
        }

        public void DeleteDataList(string name)
        {
            bool delete;
            List<Plot> list = new List<Plot>();
            foreach(Plot graph in this)
            {
                delete = false;
                foreach(string str in graph.Names)
                {
                    if (str == name) delete = true;
                }
                if (delete) list.Add(graph);
            }
            for (int i = 0; i < list.Count; i++)
            {
                this.Remove(list[i]);
            }
        }
    }
}
