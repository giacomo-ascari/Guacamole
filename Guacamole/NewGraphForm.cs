﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guacamole
{
    public partial class NewGraphForm : Form
    {
        Plot _graph = new Plot();
        DataSet _dataSet;

        public Plot Graph { get => _graph; }

        public NewGraphForm(DataSet dataSet)
        {
            InitializeComponent();
            _dataSet = dataSet;
        }

        private void content_Changed(object sender, EventArgs e)
        {
            if (nameTxt.Text.Trim().Length > 0 && xCBox.SelectedIndex > -1 && yCBox.SelectedIndex > -1)
            {
                okBtn.Enabled = true;
            } else
            {
                okBtn.Enabled = false;
            }
        }

        private void colorBtn_Paint(object sender, PaintEventArgs e)
        {
            var w = 18;
            var h = 13;
            var x = e.ClipRectangle.Left + (e.ClipRectangle.Width - w) / 2;
            var y = e.ClipRectangle.Top + (e.ClipRectangle.Height - h) / 2;
            e.Graphics.FillRectangle(new SolidBrush(Color.DarkGray), x, y, w, h);
            e.Graphics.FillRectangle(new SolidBrush(Color.WhiteSmoke), x + 1, y + 1, w - 2, h - 2);
            e.Graphics.FillRectangle(new SolidBrush(colorDialog.Color), x + 2, y + 2, w - 4, h - 4);
        }

        private void colorBtn_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                _graph.Color = colorDialog.Color;
            }
        }

        private void NewGraphForm_Load(object sender, EventArgs e)
        {
            foreach (DataList item in _dataSet)
            {
                xCBox.Items.Add(item.Name);
                yCBox.Items.Add(item.Name);
            }
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            _graph.Name = nameTxt.Text.Trim();
            _graph.Visible = visibleCBox.Checked;
            _graph.Names = new List<string>();
            _graph.Names.Add(_dataSet[xCBox.SelectedIndex].Name);
            _graph.Names.Add(_dataSet[yCBox.SelectedIndex].Name);
        }
    }
}
