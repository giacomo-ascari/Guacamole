﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Guacamole
{
    public partial class NewDataListForm : Form
    {
        DataList _dataList = null;
        DataSet _dataSet;
        public NewDataListForm(DataSet dataSet)
        {
            InitializeComponent();
            _dataSet = dataSet;
        }

        public DataList DataList { get => _dataList; }

        private void content_TextChanged(object sender, EventArgs e)
        {
            if (formatCBox.SelectedIndex > -1 && nameTxt.Text.Trim().Length > 0 && (fileRBtn.Checked || intervalRBtn.Checked) && !_dataSet.YetPresentName(nameTxt.Text.Trim())) {
                okBtn.Enabled = true;
            } else
            {
                okBtn.Enabled = false;
            }
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            if (fileRBtn.Checked)
            {
                FileForm form = new FileForm();
                form.ShowDialog();
                if (form.DialogResult == DialogResult.OK)
                {
                    try
                    {
                        _dataList = new DataList(nameTxt.Text.Trim(), (DataListFormat)formatCBox.SelectedIndex);
                        StreamReader sr = new StreamReader(form.FileName);
                        List<string> rows = new List<string>();
                        while (!sr.EndOfStream)
                        {
                            rows.Add(sr.ReadLine());
                        }
                        sr.Close();
                        for (int i = form.TopSkip; i < rows.Count - form.BottomSkip; i++)
                        {
                            string[] s = rows[i].Split(form.Separator);
                            _dataList.Add(double.Parse(s[form.Column].Replace('.', ',')));
                        }
                    } catch
                    {
                        throw new Exception();
                    }
                }
            }
            else if (intervalRBtn.Checked)
            {
                IntervalForm form = new IntervalForm();
                form.ShowDialog();
                if (form.DialogResult == DialogResult.OK)
                {
                    _dataList = new DataList(nameTxt.Text.Trim(), (DataListFormat)formatCBox.SelectedIndex, form.Start, form.Step, form.Count);
                }
            }
        }

        private void NewDataListForm_Load(object sender, EventArgs e)
        {
            formatCBox.DataSource = Enum.GetValues(typeof(DataListFormat));
        }
    }
}
