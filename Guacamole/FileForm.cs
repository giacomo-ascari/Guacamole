﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guacamole
{
    public partial class FileForm : Form
    {
        string _fileName;
        int _topSkip;
        int _bottomSkip;
        int _column;
        char _separator;

        public string FileName { get => _fileName; set => _fileName = value; }
        public int TopSkip { get => _topSkip; set => _topSkip = value; }
        public int BottomSkip { get => _bottomSkip; set => _bottomSkip = value; }
        public int Column { get => _column; set => _column = value; }
        public char Separator { get => _separator; set => _separator = value; }

        public FileForm()
        {
            InitializeComponent();
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            _topSkip = (int)topSkipNum.Value;
            _bottomSkip = (int)bottomSkipNum.Value;
            _column = (int)columnNum.Value;
            switch (separatorCombo.SelectedIndex)
            {
                case 0:
                    _separator = '\t';
                    break;
                case 1:
                    _separator = ';';
                    break;
                case 2:
                    _separator = ',';
                    break;
                case 3:
                    _separator = '.';
                    break;
            }
        }

        private void content_Changed(object sender, EventArgs e)
        {
            //MessageBox.Show(separatorCombo.SelectedIndex.ToString());
            if (_fileName.Length > 0 && (separatorCombo.SelectedIndex != -1))
            {
                okBtn.Enabled = true;
            } else
            {
                okBtn.Enabled = false;
            }
        }

        private void fileBtn_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                _fileName = openFileDialog.FileName;
                label1.Text = _fileName.Substring(_fileName.LastIndexOf((char)92) + 1, _fileName.Length - _fileName.LastIndexOf((char)92) - 1);
            } else
            {
                _fileName = string.Empty;
            }
            content_Changed(sender, e);
        }

        private void FileForm_Load(object sender, EventArgs e)
        {
            openFileDialog.FileName = string.Empty;
        }
    }
}
