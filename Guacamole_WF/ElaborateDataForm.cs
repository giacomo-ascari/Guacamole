﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guacamole_WF
{
    public partial class ElaborateDataForm : Form
    {
        private List<int> index;
        private DataSet dataSet;
        public DataSet DataSet { get => dataSet; set => dataSet = value; }


        public ElaborateDataForm(DataSet dataSet)
        {
            InitializeComponent();
            this.dataSet = dataSet;

            radioButtonConfig(averageRBtn);
            radioButtonConfig(deriveRBtn);
            radioButtonConfig(boundRBtn);
            radioButtonConfig(ratioRBtn);

            averageRBtn.Checked = true;
        }

        private void radioButtonConfig(RadioButton radioButton)
        {
            int x = radioButton.Location.X + radioButton.Parent.Location.X;
            int y = radioButton.Location.Y + radioButton.Parent.Location.Y;
            int index = this.Controls.GetChildIndex(radioButton.Parent);
            radioButton.Parent = this;
            radioButton.Location = new Point(x, y);
            this.Controls.SetChildIndex(radioButton, 0);
        }

        private void elaborateRBtn_CheckedChanged(object sender, EventArgs e)
        {
            disableGroup(averageGBox, averageRBtn.Checked);
            disableGroup(deriveGBox, deriveRBtn.Checked);
            disableGroup(boundGBox, boundRBtn.Checked);
            disableGroup(ratioGBox, ratioRBtn.Checked);
        }

        private void disableGroup(GroupBox gBox, bool a)
        {
            foreach (Control control in gBox.Controls)
            {
                control.Enabled = a;
            }
        }

        private void selectBtn_Click(object sender, EventArgs e)
        {
            SelectDataForm form = new SelectDataForm(dataSet, true);
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {
                index = form.Index;
                selectLbl.Text = "";
                foreach (int value in index)
                {
                    selectLbl.Text += dataSet[value].Name + ", ";
                }
                selectLbl.Text = selectLbl.Text.Substring(0, selectLbl.Text.Length - 2);
                okBtn.Enabled = true;
            }
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            if (averageRBtn.Checked)
            {
                if (copyCBox.Checked)
                {
                    foreach (int value in index)
                    {
                        Data data = Data.CalcAverage(dataSet[value], (int)averageSpanNum.Value, !averageKeepCBox.Checked);
                        if (data != null)
                        {
                            data.Name += " avg" + (int)averageSpanNum.Value;
                            dataSet.Add(data);
                        }
                        else MessageBox.Show("Error occured during the elaboration of '" + dataSet[value].Name + "'", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    foreach (int value in index)
                    {
                        Data data = Data.CalcAverage(dataSet[value], (int)averageSpanNum.Value, !averageKeepCBox.Checked);
                        if (data != null)
                        {
                            dataSet[value] = data;
                        }
                        else MessageBox.Show("Error occured during the elaboration of '" + dataSet[value].Name + "'", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else if (deriveRBtn.Checked)
            {
                if (copyCBox.Checked)
                {
                    foreach (int value in index)
                    {
                        Data data = Data.CalcDerive(dataSet[value], (int)deriveSpanNum.Value, !deriveKeepCBox.Checked);
                        if (data != null)
                        {
                            data.Name += " drv" + (int)deriveSpanNum.Value;
                            dataSet.Add(data);
                        }
                        else MessageBox.Show("Error occured during the elaboration of '" + dataSet[value].Name + "'", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    foreach (int value in index)
                    {
                        Data data = Data.CalcDerive(dataSet[value], (int)deriveSpanNum.Value, !deriveKeepCBox.Checked);
                        if (data != null)
                        {
                            dataSet[value] = data;
                        }
                        else MessageBox.Show("Error occured during the elaboration of '" + dataSet[value].Name + "'", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else if (boundRBtn.Checked)
            {
                if (copyCBox.Checked)
                {
                    foreach (int value in index)
                    {
                        Data data = Data.CalcBound(dataSet[value], (double)boundUpperNum.Value, (double)boundLowerNum.Value);
                        if (data != null)
                        {
                            data.Name += " bnd";
                            dataSet.Add(data);
                        }
                        else MessageBox.Show("Error occured during the elaboration of '" + dataSet[value].Name + "'", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    foreach (int value in index)
                    {
                        Data data = Data.CalcBound(dataSet[value], (double)boundUpperNum.Value, (double)boundLowerNum.Value);
                        if (data != null)
                        {
                            dataSet[value] = data;
                        }
                        else MessageBox.Show("Error occured during the elaboration of '" + dataSet[value].Name + "'", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else if (ratioRBtn.Checked)
            {
                if (copyCBox.Checked)
                {
                    foreach (int value in index)
                    {
                        double divider = 0;
                        if (dataSet[value].Count > 0 && ratioFirstRBtn.Checked) divider = dataSet[value][0];
                        else if (dataSet[value].Count > 0 && ratioLastRBtn.Checked) divider = dataSet[value][dataSet[value].Count-1];
                        else if (dataSet[value].Count > 0 && ratioCustomRBtn.Checked) divider = (double)ratioCustomNum.Value;
                        Data data = Data.CalcRatio(dataSet[value], divider);
                        if (data != null)
                        {
                            data.Name += " rt";
                            dataSet.Add(data);
                        }
                        else MessageBox.Show("Error occured during the elaboration of '" + dataSet[value].Name + "'", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    foreach (int value in index)
                    {
                        double divider = 0;
                        if (dataSet[value].Count > 0 && ratioFirstRBtn.Checked) divider = dataSet[value][0];
                        else if (dataSet[value].Count > 0 && ratioLastRBtn.Checked) divider = dataSet[value][dataSet[value].Count - 1];
                        else if (dataSet[value].Count > 0 && ratioCustomRBtn.Checked) divider = (double)ratioCustomNum.Value;
                        Data data = Data.CalcRatio(dataSet[value], divider);
                        if (data != null)
                        {
                            dataSet[value] = data;
                        }
                        else MessageBox.Show("Error occured during the elaboration of '" + dataSet[value].Name + "'", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void ratioRBtn_CheckedChanged(object sender, EventArgs e)
        {
            if (ratioCustomRBtn.Checked)
            {
                ratioCustomNum.Enabled = true;
            } else
            {
                ratioCustomNum.Enabled = false;
            }
        }
    }
}
