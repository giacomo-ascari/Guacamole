﻿namespace Guacamole_WF
{
    partial class AddDataRangeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddDataRangeForm));
            this.countNum = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.stepNum = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.firstNum = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.okBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.countNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstNum)).BeginInit();
            this.SuspendLayout();
            // 
            // countNum
            // 
            this.countNum.Location = new System.Drawing.Point(12, 117);
            this.countNum.Maximum = new decimal(new int[] {
            -727379968,
            232,
            0,
            0});
            this.countNum.Minimum = new decimal(new int[] {
            -727379968,
            232,
            0,
            -2147483648});
            this.countNum.Name = "countNum";
            this.countNum.Size = new System.Drawing.Size(313, 20);
            this.countNum.TabIndex = 22;
            this.countNum.ValueChanged += new System.EventHandler(this.range_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Count";
            // 
            // stepNum
            // 
            this.stepNum.DecimalPlaces = 6;
            this.stepNum.Increment = new decimal(new int[] {
            1,
            0,
            0,
            393216});
            this.stepNum.Location = new System.Drawing.Point(12, 70);
            this.stepNum.Maximum = new decimal(new int[] {
            -727379968,
            232,
            0,
            0});
            this.stepNum.Minimum = new decimal(new int[] {
            -727379968,
            232,
            0,
            -2147483648});
            this.stepNum.Name = "stepNum";
            this.stepNum.Size = new System.Drawing.Size(313, 20);
            this.stepNum.TabIndex = 20;
            this.stepNum.ThousandsSeparator = true;
            this.stepNum.ValueChanged += new System.EventHandler(this.range_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Step";
            // 
            // firstNum
            // 
            this.firstNum.DecimalPlaces = 6;
            this.firstNum.Increment = new decimal(new int[] {
            1,
            0,
            0,
            393216});
            this.firstNum.Location = new System.Drawing.Point(12, 26);
            this.firstNum.Maximum = new decimal(new int[] {
            -727379968,
            232,
            0,
            0});
            this.firstNum.Minimum = new decimal(new int[] {
            -727379968,
            232,
            0,
            -2147483648});
            this.firstNum.Name = "firstNum";
            this.firstNum.Size = new System.Drawing.Size(313, 20);
            this.firstNum.TabIndex = 18;
            this.firstNum.ThousandsSeparator = true;
            this.firstNum.ValueChanged += new System.EventHandler(this.range_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "First value";
            // 
            // okBtn
            // 
            this.okBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okBtn.Enabled = false;
            this.okBtn.Location = new System.Drawing.Point(168, 187);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(75, 23);
            this.okBtn.TabIndex = 24;
            this.okBtn.Text = "OK";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelBtn.Location = new System.Drawing.Point(250, 187);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 23;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            // 
            // AddDataIntervalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 222);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.countNum);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.stepNum);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.firstNum);
            this.Controls.Add(this.label4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AddDataIntervalForm";
            this.Text = "Add data as range";
            ((System.ComponentModel.ISupportInitialize)(this.countNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stepNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.firstNum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown countNum;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown stepNum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown firstNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.Button cancelBtn;
    }
}