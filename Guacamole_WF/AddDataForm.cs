﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guacamole_WF
{
    public partial class AddDataForm : Form
    {
        private List<Data> dataList;

        public AddDataForm()
        {
            InitializeComponent();
            formatCBox.DataSource = Enum.GetValues(typeof(DataFormat));
        }

        public List<Data> DataList { get => dataList; set => dataList = value; }

        private void data_TextChanged(object sender, EventArgs e)
        {
            if (nameTxt.Text.Trim().Length > 0 && formatCBox.SelectedIndex > -1 && (fileRadio.Checked || rangeRadio.Checked))
            {
                okBtn.Enabled = true;
            } else
            {
                okBtn.Enabled = false;
            }
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            if (fileRadio.Checked)
            {
                AddDataFileForm form = new AddDataFileForm(true);
                form.ShowDialog();
                if (form.DialogResult == DialogResult.OK)
                {
                    if (form.DataList != null) 
                    {
                        dataList = new List<Data>();
                        for (int i = 0; i < form.DataList.Count; i++)
                        {
                            dataList.Add(new Data(nameTxt.Text.Trim(), (DataFormat)Enum.Parse(typeof(DataFormat), formatCBox.Text), form.DataList[i]));
                            if (form.DataList.Count != 1)
                            {
                                dataList[i].Name += " (" + (i + 1) + ")";
                            }
                        }
                        //data.Add(new Data());
                        //data[0].Name = nameTxt.Text.Trim();
                        //data[0].Format = (DataFormat)Enum.Parse(typeof(DataFormat), formatCBox.Text);
                        //data.AddRange(form.Data);
                    }
                }
            }
            else if (rangeRadio.Checked)
            {
                AddDataRangeForm form = new AddDataRangeForm();
                form.ShowDialog();
                if (form.DialogResult == DialogResult.OK)
                {
                    if (form.Data != null)
                    {
                        dataList = new List<Data>();
                        dataList.Add(new Data());
                        dataList[0].Name = nameTxt.Text.Trim();
                        dataList[0].Format = (DataFormat)Enum.Parse(typeof(DataFormat), formatCBox.Text);
                        dataList[0].AddRange(form.Data);
                    }
                }
            }
        }
    }
}
