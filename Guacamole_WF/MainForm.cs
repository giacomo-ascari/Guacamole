﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Axes;

namespace Guacamole_WF
{
    public partial class MainForm : Form
    {
        Session session;
        BindingSource dataSetBinding;
        BindingSource plotSetBinding;
        

        public MainForm()
        {
            InitializeComponent();
            NewSession();    
            dataSetDGV.AutoGenerateColumns = false;
            plotSetDGV.AutoGenerateColumns = false;
            LoadDataSetDGV();
            LoadPlotSetDGV();
            LoadDataDGV();
            DrawPlotSetView();
        }
        #region PLOTVIEW METHODS and EVENTS
        public void DrawPlotSetView()
        {
            PlotSet plotset = new PlotSet(session.PlotSet.FindAll(x => x.Visible == true));
            if (plotset.Count > 0)
            {
                var model = new PlotModel();
                double minX = 0, maxX = 0;
                double minY = 0, maxY = 0;
                foreach (Plot plot in plotset)
                {
                    LineSeries series = new LineSeries() { StrokeThickness = 1, MarkerSize = 1, Title = plot.Name, Color = OxyColor.FromRgb(plot.Color.R, plot.Color.G, plot.Color.B) };
                    Data x = session.DataSet.Find(a => a.Name == plot.ListX); 
                    Data y = session.DataSet.Find(a => a.Name == plot.ListY);
                    if (x != null && y != null)
                    {
                        if (x.Max > maxX) maxX = x.Max;
                        if (x.Min < minX) minX = x.Min;
                        if (y.Max > maxY) maxY = y.Max;
                        if (y.Min < minY) minY = y.Min;
                        for (int i = 0; i < Math.Min(x.Count, y.Count); i++)
                        {
                            series.Points.Add(new DataPoint(x[i], y[i]));
                        }
                        model.Series.Add(series);
                        
                    }
                }
                model.Axes.Add(new LinearAxis
                {
                    Position = AxisPosition.Left,
                    AbsoluteMaximum = maxY / 100 * 110,
                    AbsoluteMinimum = minY,
                });
                model.Axes.Add(new LinearAxis
                {
                    Position = AxisPosition.Bottom,
                    AbsoluteMaximum = maxX,
                    AbsoluteMinimum = minX,
                });
                this.plotSetView.Model = model;
            }
            else
            {
                var model = new PlotModel();
                plotSetView.Model = model;
            }
        }

        private void splitContainer1_Panel2_SizeChanged(object sender, EventArgs e)
        {
            plotSetView.Width = splitContainer1.Panel2.Width - 5;
            plotSetView.Height = splitContainer1.Panel2.Height - 5;
        }

        #endregion


        #region DATAGRIDVIEW METHODS
        public void LoadDataSetDGV()
        {
            dataSetBinding = new BindingSource();
            dataSetBinding.DataSource = typeof(DataSet);
            dataSetBinding.DataSource = session.DataSet;
            dataSetDGV.DataSource = typeof(BindingSource);
            dataSetDGV.DataSource = dataSetBinding;
            dataSetDGV.Columns.Clear();
            dataSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Name", Name = "Name", MinimumWidth = 80 });
            dataSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Count", Name = "Count", MinimumWidth = 80 });
            dataSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Max", Name = "Max. value", MinimumWidth = 80 });
            dataSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Min", Name = "Min. value", MinimumWidth = 80 });
            dataSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Average", Name = "Average", MinimumWidth = 80 });
            dataSetDGV.Columns.Add(new DataGridViewComboBoxColumn() { DataPropertyName = "Format", Name = "Format", MinimumWidth = 80, DataSource = Enum.GetValues(typeof(DataFormat)), DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox });
        }
        public void LoadPlotSetDGV()
        {
            plotSetBinding = new BindingSource();
            plotSetBinding.DataSource = typeof(PlotSet);
            plotSetBinding.DataSource = session.PlotSet;
            plotSetDGV.DataSource = typeof(BindingSource);
            plotSetDGV.DataSource = plotSetBinding;
            plotSetDGV.Columns.Clear();
            plotSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Name", Name = "Name", MinimumWidth = 80 });
            plotSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "ListX", Name = "X axis data", MinimumWidth = 80 });
            plotSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "ListY", Name = "Y axis data", MinimumWidth = 80 });
            plotSetDGV.Columns.Add(new DataGridViewCheckBoxColumn() { DataPropertyName = "Visible", Name = "Visible", MinimumWidth = 80 });
            plotSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { Name = "Color", MinimumWidth = 80 });
        }
        public void LoadDataDGV()
        {
            dataDGV.Columns.Clear();
            dataDGV.Rows.Clear();
            for (int i = 0; i < session.DataSet.Count; i++)
            {
                dataDGV.Columns.Add(session.DataSet[i].Name, session.DataSet[i].Name);
                dataDGV.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                dataDGV.Columns[i].MinimumWidth = 80;
            }
            if (session.DataSet.MaxCount > 0)
            {
                dataDGV.Rows.Add(session.DataSet.MaxCount);
                for (int i = 0; i < dataDGV.Rows.Count; i++)
                {
                    dataDGV.Rows[i].HeaderCell.Value = (i + 1).ToString();
                }
                for (int i = 0; i < session.DataSet.Count; i++)
                {
                    for (int j = 0; j < session.DataSet[i].Count; j++)
                    {
                        dataDGV[i, j].Value = session.DataSet[i][j];
                    }
                }
            }
            FormatDataDGV();
        }
        public void FormatDataDGV()
        {
            for (int i = 0; i < session.DataSet.Count; i++)
            {
                if (session.DataSet[i].Format.ToString() != "None")
                    dataDGV.Columns[i].DefaultCellStyle.Format = session.DataSet[i].Format.ToString();
                else
                    dataDGV.Columns[i].DefaultCellStyle.Format = "";
            }
        }
        #endregion

        #region DATAGRIDVIEW EVENTS
        private void dataSetDGV_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex > 1 && e.ColumnIndex < 5 && session.DataSet.Count > 0)
            {
                //dataSetDGV.Columns[e.ColumnIndex].DefaultCellStyle.Format = "E3";
                dataSetDGV.Columns[e.ColumnIndex].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
            else if ((e.ColumnIndex == 1) && session.DataSet.Count > 0)
            {
                dataSetDGV.Columns[e.ColumnIndex].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
        }
        private void dataSetDGV_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dataSetDGV.SelectedCells[0].ColumnIndex == 5)
            {
                FormatDataDGV();
                dataSetDGV.EndEdit();
                dataSetDGV.Refresh();
            }
        }
        private void dataSetDGV_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                LoadDataDGV();
                DrawPlotSetView();
            }
        }
        private void dataSetDGV_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && e.RowIndex >= 0 && e.RowIndex < session.DataSet.Count && e.ColumnIndex == 5)
            {
                dataSetDGV.BeginEdit(true);
                ((ComboBox)dataSetDGV.EditingControl).DroppedDown = true;
            }
        }
        private void plotSetDGV_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex == 4 && e.RowIndex > -1)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                var w = 20;
                var h = 13;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;
                e.Graphics.FillRectangle(new SolidBrush(Color.DarkGray), x, y, w, h);
                e.Graphics.FillRectangle(new SolidBrush(Color.WhiteSmoke), x + 1, y + 1, w - 2, h - 2);
                e.Graphics.FillRectangle(new SolidBrush(session.PlotSet[e.RowIndex].Color), x + 2, y + 2, w - 4, h - 4);

                e.Handled = true;
            }
        }
        private void plotSetDGV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4 && e.RowIndex > -1)
            {
                if (colorDialog.ShowDialog() == DialogResult.OK)
                {
                    session.PlotSet[e.RowIndex].Color = colorDialog.Color;
                    plotSetDGV.Refresh();
                    DrawPlotSetView();
                }
            }
        }
        private void plotSetDGV_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DrawPlotSetView();
        }
        private void plotSetDGV_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (plotSetDGV.SelectedCells[0].ColumnIndex == 3)
            {
                plotSetDGV.EndEdit();
                plotSetDGV.Refresh();
            }
        }
        #endregion


        #region SESSION METHODS
        public bool SaveSession()
        {
            try
            {
                StreamWriter streamWriter = new StreamWriter(session.FileName, false);
                streamWriter.Write(session.ToString());
                streamWriter.Close();
                streamWriter.Dispose();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool SaveAsSession()
        {
            try
            {
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter streamWriter = new StreamWriter(saveFileDialog.FileName, false);
                    streamWriter.Write(session.ToString());
                    streamWriter.Close();
                    streamWriter.Dispose();
                    session.FileName = saveFileDialog.FileName;
                    UpdateTitle();
                }
                return true;
            }
            catch
            {
                return false;
            }
            
        }
        public bool LoadSession()
        {
            try
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    StreamReader streamReader = new StreamReader(openFileDialog.FileName);
                    string text = "";
                    while (!streamReader.EndOfStream)
                    {
                        text += streamReader.ReadLine() + "\n";
                    }
                    session = Session.Parse(text);
                    streamReader.Close();
                    streamReader.Dispose();
                    session.FileName = openFileDialog.FileName;
                    UpdateTitle();
                }
                return true;
            }
            catch
            {
                return false;
            }
            
        }
        public void NewSession()
        {
            session = new Session();
            UpdateTitle();
        }
        public void UpdateTitle()
        {
            string[] t1 = session.FileName.Split('\\');
            string[] t2 = t1[t1.Length - 1].Split('.');
            this.Text = "Guacamole - " + t2[0];
        }
        #endregion

        #region SESSION EVENTS
        private void newBtn_Click(object sender, EventArgs e)
        {
            if (session.FileName.Length > 0)
            {
                if (MessageBox.Show("Are you sure to create a new session? Any unsaved change will be lost.", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    NewSession();
                    LoadDataSetDGV();
                    LoadPlotSetDGV();
                    LoadDataDGV();
                    DrawPlotSetView();
                }
            } else
            {
                NewSession();
                LoadDataSetDGV();
                LoadPlotSetDGV();
                LoadDataDGV();
                DrawPlotSetView();
            }
        }
        private void saveBtn_Click(object sender, EventArgs e)
        {
            if (session.FileName.Length > 0)
            {
                if (!(File.Exists(session.FileName) && SaveSession()))
                {
                    MessageBox.Show("An error occurred while saving the session", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                if (!SaveAsSession())
                {
                    MessageBox.Show("An error occurred while saving the session", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void saveAsBtn_Click(object sender, EventArgs e)
        {
            if (!SaveAsSession())
            {
                MessageBox.Show("An error occurred while saving the session", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void loadBtn_Click(object sender, EventArgs e)
        {
            if (session.FileName.Length > 0)
            {
                if (MessageBox.Show("Are you sure to load a new session? Any unsaved change will be lost.", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    if (!LoadSession())
                    {
                        MessageBox.Show("An error occurred while loading", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        LoadDataSetDGV();
                        LoadPlotSetDGV();
                        LoadDataDGV();
                        DrawPlotSetView();
                    }

                }
            } else
            {
                if (!LoadSession())
                {
                    MessageBox.Show("An error occurred while loading", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    LoadDataSetDGV();
                    LoadPlotSetDGV();
                    LoadDataDGV();
                    DrawPlotSetView();
                }

            }
        }
        private void exitBtn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure to close this session? Any unsaved change will be lost.", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                this.Close();
            }
        }
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (session.FileName.Length > 0)
            {
                if (MessageBox.Show("Are you sure to close this session? Any unsaved change will be lost.", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                {
                    e.Cancel = true;
                }
            }
        }
        #endregion


        #region DATA METHODS and EVENTS 
        private void addData()
        {
            AddDataForm form = new AddDataForm();
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK && form.DataList != null)
            {
                session.DataSet.AddRange(form.DataList);
                LoadDataDGV();
                LoadDataSetDGV();
                DrawPlotSetView();
            }
            else if (form.DialogResult == DialogResult.OK)
            {
                MessageBox.Show("Error occured during the creation of the data.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void appendData()
        {
            AppendDataForm form = new AppendDataForm(session.DataSet);
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK && form.DataSet != null)
            {
                session.DataSet = form.DataSet;
                LoadDataDGV();
                LoadDataSetDGV();
                DrawPlotSetView();
            }
            else if (form.DialogResult == DialogResult.OK)
            {
                MessageBox.Show("Error occured during the operation.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void deleteData()
        {
            SelectDataForm form = new SelectDataForm(session.DataSet, true);
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {
                int c = 0;
                form.Index.Sort((x, y) => x.CompareTo(y));
                foreach (int i in form.Index)
                {
                    session.DataSet.RemoveAt(i - c);
                    c++;
                }
                LoadDataDGV();
                LoadDataSetDGV();
                DrawPlotSetView();
            }
        }
        private void elaborateData()
        {
            ElaborateDataForm form = new ElaborateDataForm(session.DataSet);
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK && form.DataSet != null)
            {
                session.DataSet = form.DataSet;
                LoadDataDGV();
                LoadDataSetDGV();
                DrawPlotSetView();
            }
            else if (form.DialogResult == DialogResult.OK)
            {
                MessageBox.Show("Error occured during the elaboration of the data.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void addDataMenuBtn_Click(object sender, EventArgs e)
        {
            addData();
        }
        private void appendDataMenuBtn_Click(object sender, EventArgs e)
        {
            appendData();
        }
        private void deleteDataMenuBtn_Click(object sender, EventArgs e)
        {
            deleteData();
        }
        private void elaborateMenuBtn_Click(object sender, EventArgs e)
        {
            elaborateData();
        }
        private void addDataToolBtn_Click(object sender, EventArgs e)
        {
            addData();
        }
        private void deleteDataToolBtn_Click(object sender, EventArgs e)
        {
            deleteData();
        }
        private void elaborateDataToolBtn_Click(object sender, EventArgs e)
        {
            elaborateData();
        }
        #endregion

        #region PLOT METHODS and EVENTS 
        private void addPlot()
        {
            AddPlotForm form = new AddPlotForm(session.DataSet);
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {
                session.PlotSet.AddRange(form.PlotList);
                LoadPlotSetDGV();
                DrawPlotSetView();
            }
        }
        private void deletePlot()
        {
            SelectPlotForm form = new SelectPlotForm(session.PlotSet, true);
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {
                int c = 0;
                form.Index.Sort((x, y) => x.CompareTo(y));
                foreach (int i in form.Index)
                {
                    session.PlotSet.RemoveAt(i - c);
                    c++;
                }
                LoadPlotSetDGV();
                DrawPlotSetView();
            }
        }

        private void addPlotBtn_Click(object sender, EventArgs e)
        {
            addPlot();
        }
        private void deletePlotBtn_Click(object sender, EventArgs e)
        {
            deletePlot();
        }
        private void addPlotToolBtn_Click(object sender, EventArgs e)
        {
            addPlot();
        }
        private void deletePlotToolBtn_Click(object sender, EventArgs e)
        {
            deletePlot();
        }



        #endregion
    }
}
