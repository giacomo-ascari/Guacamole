﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guacamole_WF
{
    public partial class AddPlotForm : Form
    {
        DataSet dataSet;
        int xIndex = -1;
        List<int> yIndex;
        List<Plot> plotList;

        public List<Plot> PlotList { get => plotList; }

        public AddPlotForm(DataSet dataSet)
        {
            InitializeComponent();
            this.dataSet = dataSet;
        }

        private void plot_TextChanged(object sender, EventArgs e)
        {
            if (nameTxt.Text.Trim().Length > 0 && xIndex > -1 && yIndex != null && yIndex.Count > 0)
            {
                okBtn.Enabled = true;
            } else
            {
                okBtn.Enabled = false;
            }
        }

        private void colorBtn_Click(object sender, EventArgs e)
        {
            colorDialog.ShowDialog();
        }

        private void colorBtn_Paint(object sender, PaintEventArgs e)
        {
            var w = 28;
            var h = 13;
            var x = e.ClipRectangle.Left + (e.ClipRectangle.Width - w) / 2;
            var y = e.ClipRectangle.Top + (e.ClipRectangle.Height - h) / 2;
            e.Graphics.FillRectangle(new SolidBrush(Color.DarkGray), x, y, w, h);
            e.Graphics.FillRectangle(new SolidBrush(Color.WhiteSmoke), x + 1, y + 1, w - 2, h - 2);
            e.Graphics.FillRectangle(new SolidBrush(colorDialog.Color), x + 2, y + 2, w - 4, h - 4);
        }

        private void xBtn_Click(object sender, EventArgs e)
        {
            SelectDataForm form = new SelectDataForm(dataSet, false);
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {
                xIndex = form.Index[0];
                xLabel.Text = dataSet[xIndex].Name;
            }
            form.Dispose();
        }

        private void yBtn_Click(object sender, EventArgs e)
        {
            SelectDataForm form = new SelectDataForm(dataSet, true);
            form.ShowDialog();
            if (form.DialogResult == DialogResult.OK)
            {
                yIndex = form.Index;
                yLabel.Text = "";
                foreach (int value in yIndex)
                {
                    yLabel.Text += dataSet[value].Name + ", ";
                }
                yLabel.Text = yLabel.Text.Substring(0, yLabel.Text.Length - 2);
            }
            form.Dispose();
        }

        private void okBtn_Click(object sender, EventArgs e)
        {
            try
            {
                plotList = new List<Plot>();
                for (int i = 0; i < yIndex.Count; i++)
                {
                    Plot plot = new Plot();
                    plot.Name = nameTxt.Text;
                    if (yIndex.Count > 1)
                    {
                        plot.Name += " (" + (i + 1) + ")";
                    }
                    plot.Color = colorDialog.Color;
                    plot.Visible = visibleCheck.Checked;
                    plot.ListX = dataSet[xIndex].Name;
                    plot.ListY = dataSet[yIndex[i]].Name;
                    plotList.Add(plot);
                }
            }
            catch
            {
                plotList = null;
            }
            
        }
    }
}
