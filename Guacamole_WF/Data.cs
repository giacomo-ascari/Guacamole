﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guacamole_WF
{

    public class Data : List<double>
    {
        #region Variables and Properties
        private DataFormat format;
        private string name;
        
        public DataFormat Format
        {
            get { return format; }
            set { format = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        public double Average
        {
            get
            {
                double average = 0;
                foreach (double item in this) average += item;
                return average / this.Count;
            }
        }
        public double Min
        {
            get
            {
                if (this.Count > 0)
                {
                    double min = this[0];
                    foreach (double item in this) if (item < min) min = item;
                    return min;
                }
                else return 0;
            }
        }
        public double Max
        {
            get
            {
                if (this.Count > 0)
                {
                    double max = this[0];
                    foreach (double item in this) if (item > max) max = item;
                    return max;
                }
                else return 0;
            }
        }
        #endregion

        #region Constructors
        public Data() { }

        public Data(string name, DataFormat format, List<double> list) : this()
        {
            this.name = name;
            this.format = format;
            this.AddRange(list);
        }

        public Data(string name, DataFormat format) : this()
        {
            this.name = name;
            this.format = format;
        }
        #endregion

        #region Methods
        public static Data CalcRatio(Data raw, double num)
        {
            Data final = new Data(raw.name, raw.Format);
            for (int i = 0; i < raw.Count; i++)
            {
                final.Add(num / raw[i] - 1);
            }
            return final;
        }
        public static Data CalcBound(Data raw, double upper, double lower)
        {
            Data final = new Data(raw.name, raw.Format);
            foreach(double value in raw)
            {
                if (value <= upper && value >= lower)
                {
                    final.Add(value);
                } else if (value > upper)
                {
                    final.Add(upper);
                } else if (value < lower)
                {
                    final.Add(lower);
                }
            }
            return final;
        }
        public static Data CalcDerive(Data raw, int span, bool alterCount)
        {
            Data final = new Data(raw.name, raw.Format);
            if (raw.Count >= span)
            {
                if (alterCount)
                {
                    for (int i = 0; i < raw.Count; i += span)
                    {
                        if (i + span < raw.Count)
                        {
                            final.Add((raw[i + span] - raw[i]) / span);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < raw.Count; i ++)
                    {
                        if (i + span < raw.Count)
                        {
                            final.Add((raw[i + span] - raw[i]) / span);
                        }
                    }
                }
            }
            else
            {
                final = null;
            }
            return final;
        }

        public static Data CalcAverage(Data raw, int span, bool alterCount)
        {
            Data final = new Data(raw.name, raw.Format);

            if (raw.Count >= span)
            {
                if (alterCount)
                {
                    for (int i = 0; i < raw.Count; i+=span)
                    {
                        double avg = 0;
                        int y;
                        for (y = 0; y < span && i + y < raw.Count; y++)
                        {
                            avg += raw[i + y];
                        }
                        final.Add(avg / y);
                    }
                }
                else
                {
                    for (int i = 0; i < raw.Count; i++)
                    {
                        double avg = 0;
                        int y;
                        for (y = 0; y < span && i + y < raw.Count; y++)
                        {
                            avg += raw[i + y];
                        }
                        final.Add(avg / y);
                    }
                }
            }
            else
            {
                final = null;
            }


            return final;
        }
        public static bool TryParse(string text, out Data data)
        {
            try
            {
                string[] a = text.Split(';');
                string[] b = a[2].Split(',');
                List<double> list = new List<double>();
                data = new Data(a[0], (DataFormat)int.Parse(a[1]));
                foreach (string item in b)
                {
                    data.Add(double.Parse(item.Replace('.', ',')));
                }
                return true;
            }
            catch
            {
                data = null;
                return false;
            }
        }

        public static Data Parse(string text)
        {
            Data data = null;
            if (TryParse(text, out data))
            {
                return data;
            }
            else
            {
                throw new FormatException("Invalid format");
            }
        }

        public override string ToString()
        {
            string text = string.Format("{0};{1};", name, (int)format);
            foreach (double item in this)
            {
                text += item.ToString().Replace(',', '.') + ",";
            }
            text = text.Substring(0, text.Length - 1);
            return text;
        }
        #endregion

    }

    public enum DataFormat
    {
        None,
        F0,
        F1,
        F2,
        F3,
        F6,
        F9,
        F12,
        E0,
        E1,
        E2,
        E3,
        E6,
        E9,
        E12,
    }
}
