﻿namespace Guacamole_WF
{
    partial class MainForm
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.newBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.saveBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.loadBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.exitBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.dataBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.addDataMenuBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.appendDataMenuBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteDataMenuBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.elaborateMenuBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.plotBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.addPlotBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.deletePlotBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.questionBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.helpBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutBtn = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.saveToolBtn = new System.Windows.Forms.ToolStripButton();
            this.loadToolBtn = new System.Windows.Forms.ToolStripButton();
            this.newToolBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.addDataToolBtn = new System.Windows.Forms.ToolStripButton();
            this.deleteDataToolBtn = new System.Windows.Forms.ToolStripButton();
            this.elaborateDataToolBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.addPlotToolBtn = new System.Windows.Forms.ToolStripButton();
            this.deletePlotToolBtn = new System.Windows.Forms.ToolStripButton();
            this.dataSetDGV = new System.Windows.Forms.DataGridView();
            this.plotSetDGV = new System.Windows.Forms.DataGridView();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.dataDGV = new System.Windows.Forms.DataGridView();
            this.plotSetView = new OxyPlot.WindowsForms.PlotView();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.plotSetDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataDGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileBtn,
            this.dataBtn,
            this.plotBtn,
            this.questionBtn});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(984, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileBtn
            // 
            this.fileBtn.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newBtn,
            this.saveBtn,
            this.saveAsBtn,
            this.loadBtn,
            this.exitBtn});
            this.fileBtn.Name = "fileBtn";
            this.fileBtn.Size = new System.Drawing.Size(40, 20);
            this.fileBtn.Text = "FILE";
            // 
            // newBtn
            // 
            this.newBtn.Name = "newBtn";
            this.newBtn.Size = new System.Drawing.Size(162, 22);
            this.newBtn.Text = "New session";
            this.newBtn.Click += new System.EventHandler(this.newBtn_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(162, 22);
            this.saveBtn.Text = "Save session";
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // saveAsBtn
            // 
            this.saveAsBtn.Name = "saveAsBtn";
            this.saveAsBtn.Size = new System.Drawing.Size(162, 22);
            this.saveAsBtn.Text = "Save session as...";
            this.saveAsBtn.Click += new System.EventHandler(this.saveAsBtn_Click);
            // 
            // loadBtn
            // 
            this.loadBtn.Name = "loadBtn";
            this.loadBtn.Size = new System.Drawing.Size(162, 22);
            this.loadBtn.Text = "Load session";
            this.loadBtn.Click += new System.EventHandler(this.loadBtn_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(162, 22);
            this.exitBtn.Text = "Exit";
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // dataBtn
            // 
            this.dataBtn.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addDataMenuBtn,
            this.appendDataMenuBtn,
            this.deleteDataMenuBtn,
            this.elaborateMenuBtn});
            this.dataBtn.Name = "dataBtn";
            this.dataBtn.Size = new System.Drawing.Size(48, 20);
            this.dataBtn.Text = "DATA";
            // 
            // addDataMenuBtn
            // 
            this.addDataMenuBtn.Name = "addDataMenuBtn";
            this.addDataMenuBtn.Size = new System.Drawing.Size(149, 22);
            this.addDataMenuBtn.Text = "Add new data";
            this.addDataMenuBtn.Click += new System.EventHandler(this.addDataMenuBtn_Click);
            // 
            // appendDataMenuBtn
            // 
            this.appendDataMenuBtn.Name = "appendDataMenuBtn";
            this.appendDataMenuBtn.Size = new System.Drawing.Size(149, 22);
            this.appendDataMenuBtn.Text = "Append data";
            this.appendDataMenuBtn.Click += new System.EventHandler(this.appendDataMenuBtn_Click);
            // 
            // deleteDataMenuBtn
            // 
            this.deleteDataMenuBtn.Name = "deleteDataMenuBtn";
            this.deleteDataMenuBtn.Size = new System.Drawing.Size(149, 22);
            this.deleteDataMenuBtn.Text = "Delete data";
            this.deleteDataMenuBtn.Click += new System.EventHandler(this.deleteDataMenuBtn_Click);
            // 
            // elaborateMenuBtn
            // 
            this.elaborateMenuBtn.Name = "elaborateMenuBtn";
            this.elaborateMenuBtn.Size = new System.Drawing.Size(149, 22);
            this.elaborateMenuBtn.Text = "Elaborate data";
            this.elaborateMenuBtn.Click += new System.EventHandler(this.elaborateMenuBtn_Click);
            // 
            // plotBtn
            // 
            this.plotBtn.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addPlotBtn,
            this.deletePlotBtn});
            this.plotBtn.Name = "plotBtn";
            this.plotBtn.Size = new System.Drawing.Size(47, 20);
            this.plotBtn.Text = "PLOT";
            // 
            // addPlotBtn
            // 
            this.addPlotBtn.Name = "addPlotBtn";
            this.addPlotBtn.Size = new System.Drawing.Size(145, 22);
            this.addPlotBtn.Text = "Add new plot";
            this.addPlotBtn.Click += new System.EventHandler(this.addPlotBtn_Click);
            // 
            // deletePlotBtn
            // 
            this.deletePlotBtn.Name = "deletePlotBtn";
            this.deletePlotBtn.Size = new System.Drawing.Size(145, 22);
            this.deletePlotBtn.Text = "Delete plot";
            this.deletePlotBtn.Click += new System.EventHandler(this.deletePlotBtn_Click);
            // 
            // questionBtn
            // 
            this.questionBtn.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpBtn,
            this.aboutBtn});
            this.questionBtn.Name = "questionBtn";
            this.questionBtn.Size = new System.Drawing.Size(24, 20);
            this.questionBtn.Text = "?";
            // 
            // helpBtn
            // 
            this.helpBtn.Name = "helpBtn";
            this.helpBtn.Size = new System.Drawing.Size(170, 22);
            this.helpBtn.Text = "Help";
            // 
            // aboutBtn
            // 
            this.aboutBtn.Name = "aboutBtn";
            this.aboutBtn.Size = new System.Drawing.Size(170, 22);
            this.aboutBtn.Text = "About Guacamole";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "Guacamole Session | *.gcml";
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Guacamole Session | *.gcml";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolBtn,
            this.loadToolBtn,
            this.newToolBtn,
            this.toolStripSeparator1,
            this.addDataToolBtn,
            this.deleteDataToolBtn,
            this.elaborateDataToolBtn,
            this.toolStripSeparator2,
            this.addPlotToolBtn,
            this.deletePlotToolBtn});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(984, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // saveToolBtn
            // 
            this.saveToolBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolBtn.Image = ((System.Drawing.Image)(resources.GetObject("saveToolBtn.Image")));
            this.saveToolBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolBtn.Name = "saveToolBtn";
            this.saveToolBtn.Size = new System.Drawing.Size(23, 22);
            this.saveToolBtn.Text = "Save current session";
            this.saveToolBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // loadToolBtn
            // 
            this.loadToolBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.loadToolBtn.Image = ((System.Drawing.Image)(resources.GetObject("loadToolBtn.Image")));
            this.loadToolBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.loadToolBtn.Name = "loadToolBtn";
            this.loadToolBtn.Size = new System.Drawing.Size(23, 22);
            this.loadToolBtn.Text = "Load session";
            this.loadToolBtn.Click += new System.EventHandler(this.loadBtn_Click);
            // 
            // newToolBtn
            // 
            this.newToolBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newToolBtn.Image = ((System.Drawing.Image)(resources.GetObject("newToolBtn.Image")));
            this.newToolBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolBtn.Name = "newToolBtn";
            this.newToolBtn.Size = new System.Drawing.Size(23, 22);
            this.newToolBtn.Text = "Create blank session";
            this.newToolBtn.Click += new System.EventHandler(this.newBtn_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // addDataToolBtn
            // 
            this.addDataToolBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.addDataToolBtn.Image = ((System.Drawing.Image)(resources.GetObject("addDataToolBtn.Image")));
            this.addDataToolBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addDataToolBtn.Name = "addDataToolBtn";
            this.addDataToolBtn.Size = new System.Drawing.Size(23, 22);
            this.addDataToolBtn.Text = "Add data to the session";
            this.addDataToolBtn.Click += new System.EventHandler(this.addDataToolBtn_Click);
            // 
            // deleteDataToolBtn
            // 
            this.deleteDataToolBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deleteDataToolBtn.Image = ((System.Drawing.Image)(resources.GetObject("deleteDataToolBtn.Image")));
            this.deleteDataToolBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteDataToolBtn.Name = "deleteDataToolBtn";
            this.deleteDataToolBtn.Size = new System.Drawing.Size(23, 22);
            this.deleteDataToolBtn.Text = "Delete data from the session";
            this.deleteDataToolBtn.Click += new System.EventHandler(this.deleteDataToolBtn_Click);
            // 
            // elaborateDataToolBtn
            // 
            this.elaborateDataToolBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.elaborateDataToolBtn.Image = ((System.Drawing.Image)(resources.GetObject("elaborateDataToolBtn.Image")));
            this.elaborateDataToolBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.elaborateDataToolBtn.Name = "elaborateDataToolBtn";
            this.elaborateDataToolBtn.Size = new System.Drawing.Size(23, 22);
            this.elaborateDataToolBtn.Text = "Elaborate existing data";
            this.elaborateDataToolBtn.Click += new System.EventHandler(this.elaborateDataToolBtn_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // addPlotToolBtn
            // 
            this.addPlotToolBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.addPlotToolBtn.Image = ((System.Drawing.Image)(resources.GetObject("addPlotToolBtn.Image")));
            this.addPlotToolBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addPlotToolBtn.Name = "addPlotToolBtn";
            this.addPlotToolBtn.Size = new System.Drawing.Size(23, 22);
            this.addPlotToolBtn.Text = "Add plot to the session";
            this.addPlotToolBtn.Click += new System.EventHandler(this.addPlotToolBtn_Click);
            // 
            // deletePlotToolBtn
            // 
            this.deletePlotToolBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deletePlotToolBtn.Image = ((System.Drawing.Image)(resources.GetObject("deletePlotToolBtn.Image")));
            this.deletePlotToolBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deletePlotToolBtn.Name = "deletePlotToolBtn";
            this.deletePlotToolBtn.Size = new System.Drawing.Size(23, 22);
            this.deletePlotToolBtn.Text = "Delete plot from the session";
            this.deletePlotToolBtn.Click += new System.EventHandler(this.deletePlotToolBtn_Click);
            // 
            // dataSetDGV
            // 
            this.dataSetDGV.AllowUserToAddRows = false;
            this.dataSetDGV.AllowUserToDeleteRows = false;
            this.dataSetDGV.AllowUserToResizeRows = false;
            this.dataSetDGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataSetDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataSetDGV.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataSetDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataSetDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataSetDGV.Location = new System.Drawing.Point(0, 16);
            this.dataSetDGV.MultiSelect = false;
            this.dataSetDGV.Name = "dataSetDGV";
            this.dataSetDGV.RowHeadersVisible = false;
            this.dataSetDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataSetDGV.Size = new System.Drawing.Size(525, 87);
            this.dataSetDGV.TabIndex = 2;
            this.dataSetDGV.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataSetDGV_CellFormatting);
            this.dataSetDGV.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataSetDGV_CellMouseClick);
            this.dataSetDGV.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataSetDGV_CellValueChanged);
            this.dataSetDGV.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataSetDGV_CurrentCellDirtyStateChanged);
            // 
            // plotSetDGV
            // 
            this.plotSetDGV.AllowUserToAddRows = false;
            this.plotSetDGV.AllowUserToDeleteRows = false;
            this.plotSetDGV.AllowUserToResizeRows = false;
            this.plotSetDGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.plotSetDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.plotSetDGV.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.plotSetDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.plotSetDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.plotSetDGV.Location = new System.Drawing.Point(-1, 16);
            this.plotSetDGV.MultiSelect = false;
            this.plotSetDGV.Name = "plotSetDGV";
            this.plotSetDGV.RowHeadersVisible = false;
            this.plotSetDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.plotSetDGV.Size = new System.Drawing.Size(522, 272);
            this.plotSetDGV.TabIndex = 3;
            this.plotSetDGV.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.plotSetDGV_CellClick);
            this.plotSetDGV.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.plotSetDGV_CellPainting);
            this.plotSetDGV.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.plotSetDGV_CellValueChanged);
            this.plotSetDGV.CurrentCellDirtyStateChanged += new System.EventHandler(this.plotSetDGV_CurrentCellDirtyStateChanged);
            // 
            // colorDialog
            // 
            this.colorDialog.SolidColorOnly = true;
            // 
            // dataDGV
            // 
            this.dataDGV.AllowUserToAddRows = false;
            this.dataDGV.AllowUserToDeleteRows = false;
            this.dataDGV.AllowUserToOrderColumns = true;
            this.dataDGV.AllowUserToResizeRows = false;
            this.dataDGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataDGV.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataDGV.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataDGV.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataDGV.Location = new System.Drawing.Point(0, 3);
            this.dataDGV.MultiSelect = false;
            this.dataDGV.Name = "dataDGV";
            this.dataDGV.RowHeadersVisible = false;
            this.dataDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataDGV.Size = new System.Drawing.Size(525, 179);
            this.dataDGV.TabIndex = 4;
            // 
            // plotSetView
            // 
            this.plotSetView.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.plotSetView.Location = new System.Drawing.Point(3, 3);
            this.plotSetView.Name = "plotSetView";
            this.plotSetView.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.plotSetView.Size = new System.Drawing.Size(421, 590);
            this.plotSetView.TabIndex = 5;
            this.plotSetView.Text = "plotView1";
            this.plotSetView.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.plotSetView.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.plotSetView.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.splitContainer1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.splitContainer1.Location = new System.Drawing.Point(13, 53);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer3);
            this.splitContainer1.Panel1MinSize = 300;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel2.Controls.Add(this.plotSetView);
            this.splitContainer1.Panel2.SizeChanged += new System.EventHandler(this.splitContainer1_Panel2_SizeChanged);
            this.splitContainer1.Panel2MinSize = 300;
            this.splitContainer1.Size = new System.Drawing.Size(959, 596);
            this.splitContainer1.SplitterDistance = 528;
            this.splitContainer1.TabIndex = 6;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.splitContainer2.Location = new System.Drawing.Point(-1, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer2.Panel1.Controls.Add(this.label1);
            this.splitContainer2.Panel1.Controls.Add(this.dataSetDGV);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer2.Panel2.Controls.Add(this.dataDGV);
            this.splitContainer2.Size = new System.Drawing.Size(522, 295);
            this.splitContainer2.SplitterDistance = 106;
            this.splitContainer2.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "DATASET";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "PLOTS";
            // 
            // splitContainer3
            // 
            this.splitContainer3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer3.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.splitContainer3.Location = new System.Drawing.Point(4, 4);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.splitContainer2);
            this.splitContainer3.Panel1MinSize = 50;
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer3.Panel2.Controls.Add(this.label2);
            this.splitContainer3.Panel2.Controls.Add(this.plotSetDGV);
            this.splitContainer3.Size = new System.Drawing.Size(521, 589);
            this.splitContainer3.SplitterDistance = 294;
            this.splitContainer3.TabIndex = 8;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(984, 661);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(600, 360);
            this.Name = "MainForm";
            this.Text = "Guacamole";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.plotSetDGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataDGV)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileBtn;
        private System.Windows.Forms.ToolStripMenuItem newBtn;
        private System.Windows.Forms.ToolStripMenuItem saveBtn;
        private System.Windows.Forms.ToolStripMenuItem saveAsBtn;
        private System.Windows.Forms.ToolStripMenuItem loadBtn;
        private System.Windows.Forms.ToolStripMenuItem exitBtn;
        private System.Windows.Forms.ToolStripMenuItem dataBtn;
        private System.Windows.Forms.ToolStripMenuItem plotBtn;
        private System.Windows.Forms.ToolStripMenuItem questionBtn;
        private System.Windows.Forms.ToolStripMenuItem addDataMenuBtn;
        private System.Windows.Forms.ToolStripMenuItem appendDataMenuBtn;
        private System.Windows.Forms.ToolStripMenuItem deleteDataMenuBtn;
        private System.Windows.Forms.ToolStripMenuItem addPlotBtn;
        private System.Windows.Forms.ToolStripMenuItem deletePlotBtn;
        private System.Windows.Forms.ToolStripMenuItem helpBtn;
        private System.Windows.Forms.ToolStripMenuItem aboutBtn;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.DataGridView dataSetDGV;
        private System.Windows.Forms.DataGridView plotSetDGV;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.DataGridView dataDGV;
        private OxyPlot.WindowsForms.PlotView plotSetView;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripButton saveToolBtn;
        private System.Windows.Forms.ToolStripButton loadToolBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem elaborateMenuBtn;
        private System.Windows.Forms.ToolStripButton addDataToolBtn;
        private System.Windows.Forms.ToolStripButton deleteDataToolBtn;
        private System.Windows.Forms.ToolStripButton elaborateDataToolBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton addPlotToolBtn;
        private System.Windows.Forms.ToolStripButton deletePlotToolBtn;
        private System.Windows.Forms.ToolStripButton newToolBtn;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
    }
}

