﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Guacamole_WF
{
    public class DataSet : List<Data>
    {
        public DataSet() { }


        public int MaxCount
        {
            get {
                int max = 0;
                foreach(Data data in this)
                {
                    if (data.Count > max) max = data.Count;
                }
                return max;
            }
        }


        public DataSet(List<Data> list)
        {
            this.AddRange(list);
        }

        public Data GetData(string name)
        {
            return this.Find(x => x.Name == name);
        }

        public bool DataExists(string name)
        {
            bool exists = false;
            if (this.FindIndex(x => x.Name == name) > -1) exists = true;
            return exists;
        }

        public static DataSet Parse(string text)
        {
            DataSet dataSet = new DataSet();
            if (TryParse(text, out dataSet))
            {
                return dataSet;
            } else
            {
                throw new FormatException("String format error");
            }
        }

        public static bool TryParse(string text, out DataSet dataSet)
        {
            dataSet = new DataSet();
            try
            {
                string[] v = text.Trim().Split('\n');
                foreach (string item in v)
                {
                    dataSet.Add(Data.Parse(item));
                }
                return true;
            }
            catch
            {
                dataSet = null;
                return false;
            }
        }

        public override string ToString()
        {
            string text = "";
            foreach (Data data in this)
            {
                text += data.ToString() + "\n";
            }
            return text;
        }
    }
}
