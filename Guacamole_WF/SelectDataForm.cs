﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guacamole_WF
{
    public partial class SelectDataForm : Form
    {
        DataSet dataSet;
        BindingSource dataSetBinding;
        List<int> index;
        bool multi;
        public List<int> Index { get => index; set => index = value; }

        public SelectDataForm(DataSet dataSet, bool multi)
        {
            this.multi = multi;
            this.dataSet = dataSet;
            InitializeComponent();
            LoadDataSetDGV();
            dataSetDGV.AutoGenerateColumns = false;
            dataSetDGV.MultiSelect = multi;
            if (multi)
            {
                this.Text += " (multiple selections allowed)";
            }
        }

        public void LoadDataSetDGV()
        {
            dataSetBinding = new BindingSource();
            dataSetBinding.DataSource = typeof(DataSet);
            dataSetBinding.DataSource = dataSet;
            dataSetDGV.DataSource = typeof(BindingSource);
            dataSetDGV.DataSource = dataSetBinding;
            dataSetDGV.Columns.Clear();
            dataSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Name", Name = "Name", MinimumWidth = 80 });
            dataSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Count", Name = "Count", MinimumWidth = 80 });
            dataSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Max", Name = "Max. value", MinimumWidth = 80 });
            dataSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Min", Name = "Min. value", MinimumWidth = 80 });
            dataSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Average", Name = "Average", MinimumWidth = 80 });
            dataSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Format", Name = "Format", MinimumWidth = 40 });
        }

        private void dataSetDGV_SelectionChanged(object sender, EventArgs e)
        {
            if (dataSetDGV.SelectedRows.Count > 0)
            {
                if (multi)
                {
                    index = new List<int>();
                    for (int i = 0; i < dataSetDGV.SelectedRows.Count; i++)
                    {
                        index.Add(dataSetDGV.SelectedRows[i].Index);
                    }
                } else
                {
                    index = new List<int>();
                    index.Add(dataSetDGV.SelectedRows[0].Index);
                }
                okBtn.Enabled = true;
            }
            else
            {
                okBtn.Enabled = false;
            }
        }
    }
}
