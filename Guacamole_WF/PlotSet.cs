﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guacamole_WF
{
    public class PlotSet : List<Plot>
    {
        public PlotSet() { }

        public PlotSet(List<Plot> list)
        {
            this.AddRange(list);
        }

        public static PlotSet Parse(string text)
        {
            PlotSet plotSet = new PlotSet();
            if (TryParse(text, out plotSet))
            {
                return plotSet;
            }
            else
            {
                throw new FormatException("String format error");
            }
        }

        public static bool TryParse(string text, out PlotSet plotSet)
        {
            plotSet = new PlotSet();
            try
            {
                string[] v = text.Trim().Split('\n');
                foreach (string item in v)
                {
                    plotSet.Add(Plot.Parse(item));
                }
                return true;
            }
            catch
            {
                plotSet = null;
                return false;
            }
        }

        public override string ToString()
        {
            string text = "";
            foreach (Plot item in this)
            {
                text += item.ToString() + "\n";
            }
            return text;
        }
    }
}
