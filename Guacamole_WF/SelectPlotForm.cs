﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guacamole_WF
{
    public partial class SelectPlotForm : Form
    {
        private BindingSource plotSetBinding;
        private PlotSet plotSet;
        List<int> index;
        bool multi;
        public List<int> Index { get => index; set => index = value; }

        public SelectPlotForm(PlotSet plotSet, bool multi)
        {
            this.multi = multi;
            this.plotSet = plotSet;
            InitializeComponent();
            LoadPlotSetDGV();
            plotSetDGV.AutoGenerateColumns = false;
            plotSetDGV.MultiSelect = multi;
            if (multi)
            {
                this.Text += " (multiple selections allowed)";
            }
        }

        private void plotSetDGV_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex == 4 && e.RowIndex > -1)
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All);

                var w = 20;
                var h = 13;
                var x = e.CellBounds.Left + (e.CellBounds.Width - w) / 2;
                var y = e.CellBounds.Top + (e.CellBounds.Height - h) / 2;
                e.Graphics.FillRectangle(new SolidBrush(Color.DarkGray), x, y, w, h);
                e.Graphics.FillRectangle(new SolidBrush(Color.WhiteSmoke), x + 1, y + 1, w - 2, h - 2);
                e.Graphics.FillRectangle(new SolidBrush(plotSet[e.RowIndex].Color), x + 2, y + 2, w - 4, h - 4);

                e.Handled = true;
            }
        }

        private void LoadPlotSetDGV()
        {
            plotSetBinding = new BindingSource();
            plotSetBinding.DataSource = typeof(PlotSet);
            plotSetBinding.DataSource = plotSet;
            plotSetDGV.DataSource = typeof(BindingSource);
            plotSetDGV.DataSource = plotSetBinding;
            plotSetDGV.Columns.Clear();
            plotSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Name", Name = "Name", MinimumWidth = 80 });
            plotSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "ListX", Name = "X axis data", MinimumWidth = 80 });
            plotSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "ListY", Name = "Y axis data", MinimumWidth = 80 });
            plotSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { DataPropertyName = "Visible", Name = "Visible", MinimumWidth = 80 });
            plotSetDGV.Columns.Add(new DataGridViewTextBoxColumn() { Name = "Color", MinimumWidth = 80 });
        }

        private void plotSetDGV_SelectionChanged(object sender, EventArgs e)
        {
            if (plotSetDGV.SelectedRows.Count > 0)
            {
                if (multi)
                {
                    index = new List<int>();
                    for (int i = 0; i < plotSetDGV.SelectedRows.Count; i++)
                    {
                        index.Add(plotSetDGV.SelectedRows[i].Index);
                    }
                }
                else
                {
                    index = new List<int>();
                    index.Add(plotSetDGV.SelectedRows[0].Index);
                }
                okBtn.Enabled = true;
            }
            else
            {
                okBtn.Enabled = false;
            }
        }
    }
}
